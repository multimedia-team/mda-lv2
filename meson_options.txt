# Copyright 2021-2022 David Robillard <d@drobilla.net>
# SPDX-License-Identifier: CC0-1.0 OR MIT

option('lv2dir', type: 'string', value: '', yield: true,
       description: 'LV2 bundle installation directory')

option('strict', type: 'boolean', value: false, yield: true,
       description: 'Enable ultra-strict warnings')

option('tests', type: 'feature', value: 'auto', yield: true,
       description: 'Build tests')

option('title', type: 'string', value: 'MDA.lv2',
       description: 'Project title')

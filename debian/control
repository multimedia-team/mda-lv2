Source: mda-lv2
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 Dennis Braun <d_braun@kabelmail.de>
Build-Depends:
 debhelper-compat (= 13),
 libfftw3-dev,
 libgtk-3-dev,
 libxt-dev,
 lv2-dev,
 meson,
 ninja-build,
 pkgconf,
 xsltproc
Standards-Version: 4.6.2
Homepage: https://drobilla.net/category/mdalv2/
Vcs-Git: https://salsa.debian.org/multimedia-team/mda-lv2.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/mda-lv2
Rules-Requires-Root: no

Package: mda-lv2
Provides:
 lv2-plugin
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Paul Kellett's MDA plugins ported to LV2
 This package provides Paul Kellett's MDA plugins ported to the
 LV2 specification.
 .
 LV2 (LADSPA version 2) is an open standard for plugins and matching
 host applications, mainly targeted at audio processing and generation.
 .
 This package provides the following synths and effects:
  * Ambience - Reverb effect
  * Bandisto - Multi-band distortion
  * BeatBox - Drum replacer
  * Combo - Amp & speaker simulation
  * De-ess - High frequency dynamics processor
  * Degrade - Sample quality reduction
  * Delay - Simple stereo delay with feedback tone control
  * Detune - Simple up/down pitch shifting thickener
  * Dither - Range of dither types including noise shaping
  * DubDelay - Delay with feedback saturation and time/pitch modulation
  * DX10 - FM Synthesizer
  * Dynamics - Compressor / Limiter / Gate
  * EPiano - Acoustic piano
  * Image - Stereo image adjustment and M-S matrix
  * JX10 - 2-Oscillator analog synthesizer
  * Leslie - Rotary speaker simulator
  * Limiter - Opto-electronic style limiter
  * Loudness - Equal loudness contours for bass EQ and mix correction
  * Multiband - Multi-band compressor with M-S processing modes
  * Overdrive - Soft distortion
  * Piano
  * RePsycho - Drum loop pitch changer
  * RezFilter - Resonant filter with LFO and envelope follower
  * RingMod - Ring modulator with sine-wave oscillator
  * Round Panner - 3D panner
  * Shepard - Continuously rising/falling tone generator
  * Splitter - Frequency / level crossover for setting up dynamic
    processing
  * Stereo Simulator - Haas delay and comb filtering
  * Sub-Bass Synthesizer - Several low frequency enhancement methods
  * Talkbox - High resolution vocoder
  * TestTone - Signal generator with pink and white noise, impulses
    and sweeps
  * Thru-Zero Flanger - Classic tape-flanging simulation
  * Tracker - Pitch tracking oscillator, or pitch tracking EQ
  * Transient - Transient shaper
  * VocInput - Pitch tracking oscillator for generating vocoder
    carrier input
  * Vocoder - Switchable 8 or 16 band vocoder
 .
 These plugins make no claim of compatibility, or any other relation,
 to VST. This code does not require the VST SDK, or any other non-free
 software, and it has never come into contact with any part of the VST
 SDK.
